﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_ShootAtPlayer : MonoBehaviour {
    private Combat combat;

    [SerializeField] private float searchTimer = 0.0f;
    [SerializeField] private float searchDelay = 5.0f;
    [SerializeField] private GameObject tankHead;
    [SerializeField] private float searchRadius = 10f;

    [SerializeField] private float shootTimer = 0.0f;
    [SerializeField] private float shootDelay = 5.0f;
    public bool debugMode = false;

    private GameObject currentTarget;

    [SerializeField] private Vector3 rayOffset = new Vector3(0, 0, 0);

    private void Awake() {
        combat = gameObject.GetComponent<Combat>();
    }

    private void Update() {
        //makes sure to unlock the target if they get too far away. Also unlocks the target if blocked by a building.
        if (Physics.Raycast(new Ray(tankHead.transform.position, tankHead.transform.forward), out RaycastHit hit, searchRadius)) {
            if (hit.collider.gameObject.tag != "Player") {
                currentTarget = null;
            }

            if (debugMode) //shows the raytrace line if required
            {
                Debug.DrawRay(tankHead.transform.position, tankHead.transform.forward * searchRadius, new Color(0, 0, 0));
            }
        }
        else { currentTarget = null; }

        if (currentTarget == null) {
            RunIdleAnimation();
            if (searchTimer > searchDelay) {
                LookForEnemy();
                searchTimer = 0.0f;
            }
            searchTimer += Time.deltaTime;

        }

        if (currentTarget != null) {
            LookAtEnemy();
            if (shootTimer > shootDelay) {
                combat.Shoot();
                shootTimer = 0.0f;
            }
            shootTimer += Time.deltaTime;
        }
    }

    private void LookForEnemy() {

        //look for entity tagged "player"
        Collider[] hit = Physics.OverlapSphere(gameObject.transform.position, searchRadius);

        if (debugMode) {
        }

        for (int i = 0; i < hit.Length; i++) {
            if (hit[i].CompareTag("Player")) {
                currentTarget = hit[i].gameObject;
            }
        }
    }

    private void LookAtEnemy() {
        tankHead.transform.LookAt(currentTarget.transform);
        tankHead.transform.Rotate(rayOffset);
    }

    private void RunIdleAnimation() {
        tankHead.transform.rotation = Quaternion.identity;
    }
}
