﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    public enum GameState { setup, gameLoop, end }
    public string currentLevel;

    public GameState gameState = GameState.setup;
    public GameObject player;
    public GameObject[] spawns;
    public GameObject enemy;
    public float spawnDelay = 5.0f;
    public float diffiulctyMod = 0.5f;
    public float timer = 0.0f;
    public float respawnTimer = 0.0f;
    public float respawnDelay = 5.0f;

    private void Awake() {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update() {

        switch (gameState) {
            case GameState.setup:
                state_setup();
                break;
            case GameState.gameLoop:
                state_gameLoop();
                break;
            case GameState.end:
                state_end();
                break;
        }
    }

    void state_setup() //expensive stuff
    {
        spawns = GameObject.FindGameObjectsWithTag("Spawn"); //expensive
        gameState = GameState.gameLoop;

    }

    void state_gameLoop() {
        GameObject spawnLocation = spawns[Random.Range(0, spawns.Length)];
        timer += Time.deltaTime;
        if (timer > (spawnDelay / diffiulctyMod)) {
            Instantiate(enemy, spawnLocation.transform.position, spawnLocation.transform.rotation);
            timer = 0;
        }
        if (player == null) {
            respawnTimer += Time.deltaTime;
            if (respawnTimer > respawnDelay) {
                respawnTimer = 0;
                SceneManager.LoadScene(currentLevel);
            }
        }
    }
    void state_end() {}
}
