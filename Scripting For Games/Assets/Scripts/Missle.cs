﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missle : MonoBehaviour {
    public float speed = 5.0f;
    public ParticleSystem explosion;
    [SerializeField] private int damage;

    private void Update() { transform.Translate(Vector3.forward * Time.deltaTime * speed); }

    private void OnTriggerEnter(Collider other) {
        Entity haveComp = other.GetComponent<Entity>();

        if (haveComp != null)
            haveComp.TakeDamage(damage);

        Destroy(this.gameObject);
        Instantiate(explosion, this.transform.position, this.transform.rotation);
    }
}