﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]

public class Combat : MonoBehaviour {

    public Transform shootFrom;
    public GameObject misslePrefab;
    public Transform tankHead;
    public AudioSource adios;

    private void Awake() {
        adios = GetComponent<AudioSource>();
    }

    private void Update() {
        //tankHead.rotation = cameraTransform.rotation;

    }

    public void Shoot() {
        Instantiate(misslePrefab, shootFrom.position, tankHead.rotation);
        adios.Play();
    }

    public void RecieveRotation(Quaternion _newRotation) {
        tankHead.rotation = _newRotation;
    }
}
