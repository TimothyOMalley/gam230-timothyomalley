﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    public float speed = 3.0f;
    public float rotationSpeed = 100f;
    private Vector2 inputV;
    Rigidbody rb;

    void Awake() {
        rb = GetComponent<Rigidbody>();
    }

    public void giveInfo(Vector2 _input) {
        inputV = _input;
    }

    private void Update() {
        transform.Rotate(transform.up * inputV.x * rotationSpeed * Time.deltaTime);
    }

    private void FixedUpdate() {
        rb.MovePosition(rb.position + (transform.forward * inputV.y) * speed * Time.fixedDeltaTime);
    }
}
