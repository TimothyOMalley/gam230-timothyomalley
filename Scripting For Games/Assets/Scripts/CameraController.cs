﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Transform target;
    public Vector3 followOffset;
    public float rotationSpeed = 250.0f;

    private void Awake() {
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }
    private void Update() {
        //rotational input
        float mouseX = Input.GetAxis("Mouse X");
        transform.Rotate(transform.up * mouseX * rotationSpeed * Time.deltaTime);

        transform.position = target.position + followOffset;
    }
}
