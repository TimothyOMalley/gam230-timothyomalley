﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterTime : MonoBehaviour {
    float timepassed;
    public float timeBeforeDestruction;
    public GameObject selfObj;
    private void Update() {
        timepassed += Time.deltaTime;
        if (timepassed >= timeBeforeDestruction)
            Destroy(selfObj);
    }
}
