﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishZone : MonoBehaviour {

    public string NextScene;

    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Player")
            SceneManager.LoadScene(NextScene);
    }

    public void load(string TheScene) { SceneManager.LoadScene(TheScene); }

    public void quit() { Application.Quit(); }
}
