﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Light))]
[RequireComponent(typeof(ParticleSystem))]

public class ComponentManager : MonoBehaviour {
    public Transform offset;
    public GameObject managedObj;
    public AudioSource soundByte;
    public ParticleSystem particleFX;

    private Transform offset_;
    private GameObject managedObj_;
    private AudioSource soundByte_;
    private ParticleSystem particleFX_;

    private void Awake() {
        offset_ = offset.GetComponent<Transform>();
        managedObj_ = managedObj;
        soundByte_ = soundByte.GetComponent<AudioSource>();
        particleFX_ = particleFX.GetComponent<ParticleSystem>();

        offset_.Translate(1f, 1f, 1f);
        managedObj_.tag = "Finish";
        soundByte_.volume = 5;
        particleFX_.Stop();
    }
}
