﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Movement))]
[RequireComponent(typeof(Combat))]
public class PlayerInput : MonoBehaviour {
    private Movement move;
    private Combat combat;
    public Transform cameraTransform;
    private bool canShoot = true;
    private float shootDelay = 1.2f;

    private void Awake() {
        move = gameObject.GetComponent<Movement>();
        combat = gameObject.GetComponent<Combat>();
        cameraTransform = GameObject.FindGameObjectWithTag("MainCamera").transform;
    }

    private void Update() {
        Vector2 movementInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        if (movementInput.magnitude >= 1.0f)
            movementInput = movementInput.normalized;

        move.giveInfo(movementInput);
        combat.RecieveRotation(cameraTransform.rotation);

        if (canShoot) {

            if (Input.GetButtonDown("Fire1")) {
                combat.Shoot();
                canShoot = false;
            }
        }
        else {
            shootDelay -= Time.deltaTime;
            if (shootDelay <= 0f) {
                shootDelay = 1.2f;
                canShoot = true;
            }
        }
    }
}
