﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour {
    [SerializeField] private GameObject spawnOnDeath;

    public int Health = 10;

    public void TakeDamage(int _Damage) {
        Health -= _Damage;

        if (Health <= 0)
            Die();
    }

    public void Die() {
        Destroy(gameObject);

        if (spawnOnDeath != null)
            Instantiate(spawnOnDeath, transform.position, Quaternion.identity);
    }
}

