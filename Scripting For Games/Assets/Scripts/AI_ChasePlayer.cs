﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Movement))]

public class AI_ChasePlayer : MonoBehaviour {
    private GameObject currentTarget;
    private Movement mvmnt;
    private Ray ray;
    public RaycastHit hit;
    [SerializeField] private Vector3 offset;
    [SerializeField] private float stopDistance = 0.0f;

    public bool debugMode = false;

    private void Awake() {
        mvmnt = gameObject.GetComponent<Movement>();
    }

    public void Update() {
        if (debugMode) {
            Debug.DrawLine(transform.position, transform.forward * 100, new Color(0, 0, 0));
        }
        ray = new Ray(transform.position + offset, transform.forward);
        if (currentTarget == null) {
            LookForEnemy();
        }
        else {
            if (Physics.Raycast(ray, out hit, 100)) {
                if (hit.collider.tag == "Player") {
                    if (hit.distance > stopDistance) {
                        mvmnt.giveInfo(new Vector2(0, 1));
                    }
                    else { mvmnt.giveInfo(new Vector2(0, 0)); }
                }
                else {
                    LookAtEnemy();
                }
            }
        }
    }

    private void LookForEnemy() {
        currentTarget = GameObject.FindGameObjectWithTag("Player");
    }

    private void LookAtEnemy() {
        gameObject.transform.LookAt(currentTarget.transform);
    }
}
